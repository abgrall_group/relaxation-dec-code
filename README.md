# Relaxation DeC
## Public repository for relaxation Deferred Correction (RDeC) method

[![License: MIT](https://img.shields.io/badge/License-MIT-success.svg)](https://opensource.org/licenses/MIT)

In this repository we provide a *Python* and a *Julia* implementation of the RDeC method: an arbitrarily high order accurate time integration method that allows to preserve (or dissipate) entropies for ODEs and simple hyperbolic PDEs. This repository provides the code used in the work **Relaxation Deferred Correction Methods  and their Applications to Residual Distribution Schemes** by R. Abgrall, E. Le Mélédo, P. Offner and D. Torlo available at this [arXiv link](https://arxiv.org/abs/2106.05005).


In the [python notebook](Notebooks/python/RelaxationDeC.ipynb) there is an extension of the [notebook](https://github.com/ketch/RRK_rr) proposed by H. Ranocha, M. Sayyari, L. Dalcin, M. Parsani and D. I. Ketcheson. There the DeC is written into a Runge--Kutta form and it is used to solve the same tests proposed by the original relaxation Runge--Kutta work. The file with the DeC related functions, including the passage to Runge--Kutta methods is [here](Notebooks/python/DeC.py).

In the [Julia notebook](Notebooks/julia/RelaxationDeC.ipynb) an implementation of the relaxation DeC, without recurring to a Runge--Kutta algorithm, is provided, in particular in [Relaxation DeC implementation](Notebooks/julia/DeC.jl). In this notebook only energy conservative/dispersive tests are considered. We test the convergence of RDeC up to 8th order in this notebook. 


## Disclaimer

Everything is provided as-is and without warranty. Use at your own risk!
