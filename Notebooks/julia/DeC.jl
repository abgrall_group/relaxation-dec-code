#using Plots
using FastGaussQuadrature, Compat
using LinearAlgebra
using Polynomials

"""Different models"""

function lagrange_basis(X,t,i)
    idxs = eachindex(X)
    if t in X
        if t == X[i]
            return 1
        else
            return 0
        end
    end
    return prod((t-X[j])/(X[i]-X[j]) for j in idxs if j != i)
end

            
function equispaced(order)
    nodes=range(-1,stop=1,length=order)
    w=zeros(order)
    for k=1:order
        zz=Polynomial([1.])
        for s=1:order
            if s == k
                continue
            else
                zz=zz* Polynomial([-nodes[s],1])
            end
        end
        zz=zz/zz(nodes[k])
        intzz=integrate(zz)
        w[k]=intzz(1)-intzz(-1)
    end
    return nodes, w
end
     

function get_nodes(order,nodes_type)
    if nodes_type=="equispaced"
        nodes, w = equispaced(order)
    elseif nodes_type=="gaussLobatto"
        nodes, w = gausslobatto(order)
    elseif nodes_type=="gaussLegendre"
        nodes, w = gausslegendre(order)
    end
    w = map( x-> x*0.5, w)
    nodes = nodes*0.5.+0.5    
    return nodes, w
end


function compute_theta_DeC(order, nodes_type)
    
    nodes, w = get_nodes(order,nodes_type)
    int_nodes, int_w = get_nodes(order,"gaussLobatto")
    # generate theta coefficients 
    theta = zeros(order,order)
    beta_m =zeros(order)
    for m in 1:order
        beta_m[m] =nodes[m]
        nodes_m = int_nodes*(nodes[m])
        w_m = int_w*(nodes[m])
        for r in 1:order
            theta[r,m] = sum([lagrange_basis(nodes,nodes_m[k],r)*w_m[k] for k in 1:order])
        end
    end
    return theta, beta_m
    
end
            
function compute_theta_DeC_subtimesteps(order, nodes_type)
    
    nodes, w = get_nodes(order,nodes_type)
    int_nodes, int_w = get_nodes(order,"gaussLobatto")
    # generate theta coefficients 
    theta = zeros(order,order)
    beta_m =zeros(order)
    for m in 1:order
        if m==1
            beta_m[m] = nodes[1]
            nodes_m=int_nodes*beta_m[m] 
            w_m = int_w*beta_m[m]
        else
            beta_m[m] = (nodes[m]-nodes[m-1])
            nodes_m = int_nodes*beta_m[m] .+nodes[m-1]
            w_m = int_w*beta_m[m]
        end
        for r in 1:order
            theta[r,m] = sum([lagrange_basis(nodes,nodes_m[k],r)*w_m[k] for k in 1:order])
        end
    end
    return theta, beta_m
    
end


""" Computing the matrix of the values T_k^m = int_{t^0}^{t^m} varphi_k"""
function calculate_theta(M_sub::Int, distribution)
  if distribution=="equispaced"
    nodes=range(0,stop=1, length=M_sub+1)
  elseif distribution=="gaussLobatto"
    nodes,p = gausslobatto(M_sub+1)
    nodes=nodes*0.5.+0.5
  end
  theta=compute_theta(nodes)
  return theta
end


#Dec_correction-
#Func is the function of the ode
#N_step -> Number of subtimesteps
#Corr-> Number of Corrections
#y_0 Initial condition
# t_0 Start value
# t_end End value
# Maybe logical variable for checking for the different solvers or the distributaiton
# of subtimesteps up to some order
#Distribution is set equidistand or different order

""" Dec_correction constant time step"""
function dec(func, tspan, y_0, M_sub::Int, K_corr::Int, distribution)

   N_time=length(tspan)
   dim=length(y_0)
   U=zeros(dim, N_time)
   u_p=zeros(dim, M_sub+1)
   u_a=zeros(dim, M_sub+1)
   u_help=zeros(dim)
   func_corr=zeros(dim,M_sub+1)
   Theta, beta = compute_theta_DeC(M_sub+1,distribution)
   U[:,1]=y_0
   for it=2: N_time
        delta_t=(tspan[it]-tspan[it-1])
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r])
            end
            for m=2:M_sub+1
            u_a[:,m]= u_a[:,1]+delta_t*sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1)
            end
        end
        U[:,it]=u_a[:,M_sub+1]
    end
    return tspan, U
end
            
function dec_subtimesteps(func, tspan, y_0, M_sub::Int, K_corr::Int, distribution)

   N_time=length(tspan)
   dim=length(y_0)
   U=zeros(dim, N_time)
   u_p=zeros(dim, M_sub+1)
   u_a=zeros(dim, M_sub+1)
   u_help=zeros(dim)
   func_corr=zeros(dim,M_sub+1)
   Theta, beta = compute_theta_DeC_subtimesteps(M_sub+1,distribution)
   U[:,1]=y_0
   for it=2: N_time
        delta_t=(tspan[it]-tspan[it-1])
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r])
            end
            for m=2:M_sub+1
            u_a[:,m]= u_a[:,m-1]+delta_t*sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1)
            end
        end
        U[:,it]=u_a[:,M_sub+1]
    end
    return tspan, U
end

""" Dec_correction constant time step""" 
function energy_relaxed_dec(func, t0, t_end, dt0, y_0, M_sub::Int, K_corr::Int, distribution)

   dim=length(y_0)
   U=zeros(dim,1)
   U[:,1]=y_0
   time=t0
   tSpan=t0
   gammas=1.0
   it=1
   u_p=zeros(dim, M_sub+1)
   u_a=zeros(dim, M_sub+1)
   u_help=zeros(dim)
   func_corr=zeros(dim,M_sub+1)
   Theta, beta = compute_theta_DeC(M_sub+1,distribution)
   while (time <t_end)
        it=it+1
        delta_t=dt0
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r])
            end
            for m=2:M_sub+1
            u_a[:,m]= u_a[:,1]+delta_t*sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1)
            end
        end
        udiff=u_a[:,M_sub+1]-U[:,it-1]
        gamma=-2*(udiff'*U[:,it-1])/(udiff'*udiff)
        delta_t_relaxed=gamma*delta_t
        time= time+delta_t_relaxed
        tSpan=[tSpan time]
        gammas=[gammas gamma]
        uRelaxed = U[:,it-1]+gamma*udiff
        U=[U uRelaxed]
    end
    return tSpan, U, gammas
end

""" Dec_correction constant time step""" 
function energy_relaxed_dec_dissipative(func, t0, t_end, dt0, y_0, M_sub::Int, K_corr::Int, distribution)

   dim=length(y_0)
   U=zeros(dim,1)
   U[:,1]=y_0
   time=t0
   tSpan=t0
   gammas=1.0
   it=1
   u_p=zeros(dim, M_sub+1)
   u_a=zeros(dim, M_sub+1)
   u_help=zeros(dim)
   func_corr=zeros(dim,M_sub+1)
   Theta, beta = compute_theta_DeC(M_sub+1,distribution)
   while (time <t_end)
        it=it+1
        delta_t=dt0
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r])
            end
            for m=2:M_sub+1
                u_a[:,m]= u_a[:,1]+delta_t*sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1)
            end
        end
        udiff=u_a[:,M_sub+1]-U[:,it-1]
        gamma=2*delta_t*sum(Theta[r,M_sub+1]*func_corr[:,r]'*(u_p[:,r]-U[:,it-1])  for r in 1:M_sub+1)/(udiff'*udiff)
        delta_t_relaxed=gamma*delta_t
        time= time+delta_t_relaxed
        tSpan=[tSpan time]
        gammas=[gammas gamma]
        uRelaxed = U[:,it-1]+gamma*udiff
        U=[U uRelaxed]
    end
    return tSpan, U, gammas
end
      

function dec_stiff(func, jac_stiff, tspan, y_0, M_sub::Int, K_corr::Int, distribution)
   verbose=0
   N_time=length(tspan)
   dim=length(y_0)
   U=zeros(dim, N_time)
   u_p=zeros(dim, M_sub+1)
   u_a=zeros(dim, M_sub+1)
   u_help=zeros(dim)
   func_corr=zeros(dim,M_sub+1)
   Theta, beta = compute_theta_DeC(M_sub+1,distribution)
   invJac=zeros(M_sub+1,dim,dim)
   U[:,1]=y_0
   for it=2: N_time
        delta_t=(tspan[it]-tspan[it-1])
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        SS=jac_stiff(u_p[:,1])
        for m=2:M_sub+1
            invJac[m,:,:]=inv(Matrix(I,dim,dim).+ delta_t*beta[m] .*SS)
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r])
            end
            for m=2:M_sub+1
                u_a[:,m]= u_p[:,m]+delta_t.*invJac[m,:,:]*(-(u_p[:,m]-u_p[:,1])./delta_t+sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1))
            end
        if verbose==1
            println("dt $(delta_t)")
            println("up $(u_p)")
            println("jac $(jac_stiff(u_p[:,1]))")
            println("invJac $(invJac)")
        end
        end
        U[:,it]=u_a[:,M_sub+1]
    end
    return tspan, U
end

            
                        
function dec_stiff_subtimesteps(func, jac_stiff, tspan, y_0, M_sub::Int, K_corr::Int, distribution)
   verbose=0
   N_time=length(tspan)
   dim=length(y_0)
   U=zeros(dim, N_time)
   u_p=zeros(dim, M_sub+1)
   u_a=zeros(dim, M_sub+1)
   u_help=zeros(dim)
   func_corr=zeros(dim,M_sub+1)
   Theta, beta = compute_theta_DeC_subtimesteps(M_sub+1,distribution)
   S = zeros(dim,dim)
   invJac=zeros(M_sub+1,dim,dim)
   U[:,1]=y_0
   for it=2: N_time
        delta_t=(tspan[it]-tspan[it-1])
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        SS=jac_stiff(u_p[:,1])
        for m=1:M_sub+1
            invJac[m,:,:]=inv(Matrix(I,dim,dim) .+ delta_t*beta[m] .*SS)
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r])
            end
            for m=2:M_sub+1
                L2=(u_p[:,m]-u_a[:,m-1])-delta_t.*sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1) #
                u_a[:,m]= u_p[:,m]+ invJac[m,:,:]*( - L2) #u_a[:,m-1]-u_p[:,m-1]
                #u_a[:,m]= invJac[m,:,:]*(u_a[:,m-1] +delta_t*beta[m] .*SS*u_p[:,m] -L2)
            end
        if verbose==1
                            println("beta $(beta)")
                            println("theta $(Theta)")
            println("dt $(delta_t)")
            println("up $(u_p)")
            println("jac $(jac_stiff(u_p[:,1]))")
            println("invJac $(invJac)")
        end
        end
        U[:,it]=u_a[:,M_sub+1]
    end
    return tspan, U
end


